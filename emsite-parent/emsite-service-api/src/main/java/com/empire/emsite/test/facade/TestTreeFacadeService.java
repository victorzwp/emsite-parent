/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.test.facade;

import java.util.List;

import com.empire.emsite.test.entity.TestTree;

/**
 * 类TestTreeFacadeService.java的实现描述：树结构生成FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:58:24
 */
public interface TestTreeFacadeService {

    public TestTree get(String id);

    public List<TestTree> findList(TestTree testTree);

    public void save(TestTree testTree);

    public void delete(TestTree testTree);

}
