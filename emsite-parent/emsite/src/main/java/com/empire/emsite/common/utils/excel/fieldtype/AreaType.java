/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.utils.excel.fieldtype;

import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.modules.sys.entity.Area;
import com.empire.emsite.modules.sys.utils.UserUtils;

/**
 * 类AreaType.java的实现描述：字段类型转换
 * 
 * @author arron 2017年10月30日 下午7:05:04
 */
public class AreaType {

    /**
     * 获取对象值（导入）
     */
    public static Object getValue(String val) {
        for (Area e : UserUtils.getAreaList()) {
            if (StringUtils.trimToEmpty(val).equals(e.getName())) {
                return e;
            }
        }
        return null;
    }

    /**
     * 获取对象值（导出）
     */
    public static String setValue(Object val) {
        if (val != null && ((Area) val).getName() != null) {
            return ((Area) val).getName();
        }
        return "";
    }
}
