package com.empire.emsite.common.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.empire.emsite.common.utils.Reflections;
import com.empire.emsite.common.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 类BaseTreeDTO.java的实现描述：tree实体类型通用传输层DTO
 * 
 * @author arron 2018年3月23日 下午4:08:52
 */
public abstract class BaseTreeDTO<T> extends BaseDataDTO {
    private static final long serialVersionUID = 1L;
    private T                 parent;               // 父级编号
    private String            parentIds;            // 所有父级编号
    private String            name;                 // 机构名称
    private Integer           sort;                 // 排序

    public BaseTreeDTO() {
        super();
        this.sort = 30;
    }

    public BaseTreeDTO(String id) {
        super(id);
    }

    /**
     * 父对象，只能通过子类实现，父类实现mybatis无法读取
     * 
     * @return
     */
    @JsonBackReference
    @NotNull
    public abstract T getParent();

    /**
     * 父对象，只能通过子类实现，父类实现mybatis无法读取
     * 
     * @return
     */
    public abstract void setParent(T parent);

    @Length(min = 1, max = 2000)
    public String getParentIds() {
        return parentIds;
    }

    public void setParentIds(String parentIds) {
        this.parentIds = parentIds;
    }

    @Length(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getParentId() {
        String id = null;
        if (parent != null) {
            id = (String) Reflections.getFieldValue(parent, "id");
        }
        return StringUtils.isNotBlank(id) ? id : "0";
    }

}
